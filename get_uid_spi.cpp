#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <string.h>
#include "pn532_spi.h"

using namespace pn532;

#define IOCTL(fd, cmd, arg) \
	do\
	{\
		if (ioctl(fd, cmd, arg) == -1)\
		{\
			perror(#cmd);\
			close(fd);\
			return -1;\
		}\
	}\
	while(0)

void reverse_byte(uint8_t & b)
{
	uint8_t res = 0;
	uint8_t ori = b;
	for (int i = 0; i < 8; ++i)
	{
		res <<= 1;
		res |= ori & 0x01;
		ori >>= 1;
	}
	b = res;
}

void reverse_bits(uint8_t * buf, int len)
{
	uint8_t* p = buf;
	for (int i = 0; i < len; ++i)
	{
		reverse_byte(*p++);
	}
}

int init_spi(const char * dev)
{
	static const uint8_t spi_mode = 0;
	static const uint8_t spi_lsb = 1;
	static const uint8_t spi_bpw = 8;
	//static const uint32_t spi_speed = 5000000;
	static const uint32_t spi_speed = 1000000;

	int fd = open(dev, O_RDWR);
	if (fd == -1)
	{
		perror("open spi device:");
		return -1;
	}
	
	IOCTL(fd, SPI_IOC_WR_MODE, &spi_mode);
	//IOCTL(fd, SPI_IOC_WR_LSB_FIRST, &spi_lsb);
	IOCTL(fd, SPI_IOC_WR_BITS_PER_WORD, &spi_bpw);
	IOCTL(fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi_speed);

	return fd;
}

int sr_spi(int spi, const uint8_t* sendbuf, int sendlen, uint8_t* recvbuf, int recvlen)
{
	int trnum = 0;
	struct spi_ioc_transfer tr[2];
	memset(tr, 0, sizeof(tr));
	frame_ptr lsbbuf;
	if (sendlen > 0)
	{
		lsbbuf = new_frame(sendlen);
		memcpy(lsbbuf.get(), sendbuf, sendlen);
		reverse_bits(lsbbuf.get(), sendlen);
		tr[trnum].tx_buf = (uint64_t)lsbbuf.get();
		tr[trnum].rx_buf = 0;
		tr[trnum].len = sendlen;
		++trnum;
	}
	if (recvlen > 0)
	{
		tr[trnum].tx_buf = 0;
		tr[trnum].rx_buf = (uint64_t)recvbuf;
		tr[trnum].len = recvlen;
		++trnum;
	}
	if (trnum == 0)
	{
		return -1;
	}
	if (ioctl(spi, SPI_IOC_MESSAGE(trnum), tr) != (sendlen + recvlen))
	{
		perror("spi send recv error:");
		return -1;
	}
	if (recvlen > 0)
	{
		reverse_bits(recvbuf, recvlen);
	}
	return sendlen+recvlen;
}

int send_spi(int spi, const uint8_t* sendbuf, int sendlen)
{
	return sr_spi(spi, sendbuf, sendlen, NULL, 0);
}

int recv_spi(int spi, uint8_t* recvbuf, int recvlen)
{
	return sr_spi(spi, NULL, 0, recvbuf, recvlen);
}

void print_hex(const uint8_t * buf, int len)
{
	const uint8_t * p = buf;
	for (int i = 0; i < len; ++i)
	{
		printf("%02X ", *p++);
		if ((i+1) % 16 == 0)
		{
			printf("\n");
		}
	}
	if (len % 16 != 0)
	{
		printf("\n");
	}
}

int wait_for_ready(int spi)
{
	static const uint8_t sr[] = {pn532::SPI_SR};
	uint8_t buf[1];
	while (1)
	{
		usleep(10000);
		if (sr_spi(spi, sr, 1, buf, 1) != 2)
		{
			printf("spi send recv error!\n");
			return -1;
		}
		if (buf[0] == 0x01)
			return 0;
	}
	return -1;
}

#define BUF_SIZE 264

int do_cmd(int spi, const uint8_t* cmd, int cmdlen, const char* cmdname, int answerlen)
{
	static const uint8_t dr[] = {pn532::SPI_DR};

	if (cmdname != NULL)
	{
		printf("do command: %s\n", cmdname);
	}

	if (answerlen < 0)
	{
		answerlen = BUF_SIZE;
	}

	uint8_t buf[BUF_SIZE];
	frame_ptr cmd_frame;
	int cmd_frame_len = make_spi_frame(cmd_frame, SPI_DW, TFI_H2P, cmd, cmdlen);
	print_hex(cmd_frame.get(), cmd_frame_len);
	if (send_spi(spi, cmd_frame.get(), cmd_frame_len) != cmd_frame_len)
	{
		perror("write cmd");
		return -1;
	}
	if (wait_for_ready(spi) != 0)
	{
		return -1;
	}
	printf("ready for ack!\n");
	if (sr_spi(spi, dr, 1, buf, 6) < 0)
	{
		perror("read ack error");
		return -1;
	}
	printf("read ack: ");
	print_hex(buf, 6);
	if (wait_for_ready(spi) != 0)
	{
		return -1;
	}
	printf("ready for answer!\n");
	if (sr_spi(spi, dr, 1, buf, answerlen) < 0)
	{
		perror("read answer error");
		return -1;
	}
	printf("read answer: ");
	print_hex(buf, answerlen);

	return 0;
}

int main(int argc, char *argv[])
{
	int spi = init_spi("/dev/spidev0.0");
	if (spi < 0)
	{
		fprintf(stderr, "init spi device error!\n");
		return -1;
	}
	
#if 0
	uint8_t gfv_data[] = {0x02};
	if (do_cmd(spi, gfv_data, sizeof(gfv_data), "get fireware version", 13) < 0)
	{
		return -1;
	}
#endif

#if 1
	uint8_t set_normal_mode_data[] = {0x14, 0x01, 0x00, 0x00};
	if (do_cmd(spi, set_normal_mode_data, sizeof(set_normal_mode_data), "set normal mode", 9) < 0)
	{
		return -1;
	}
#endif

#if 1
	uint8_t autopoll_data[] = {0x60, 0xff, 0x02, 0x00, 0x01, 0x02, 0x03, 0x04, 0x10, 0x11, 0x12, 0x20, 0x23};
	if (do_cmd(spi, autopoll_data, sizeof(autopoll_data), "auto poll", 32) < 0)
	{
		return -1;
	}
#endif

#if 0
	uint8_t lpt_data[] = {0x4A, 0x01, 0x00};
	if (do_cmd(spi, lpt_data, sizeof(lpt_data), "list passive target", 32) < 0)
	{
		return -1;
	}
#endif

	close(spi);
	return 0;
}
