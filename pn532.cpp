#include "pn532.h"
#include <cassert>

namespace pn532
{

uint8_t checksum(const uint8_t * data, int data_len)
{
	assert(data_len >= 0);
	uint8_t cs = 0;
	const uint8_t* p = data;
	for (int i = 0; i < data_len; ++i)
	{
		cs += *p++;
	}
	cs = (~cs) + 1;
	return cs;
}

uint8_t checksum(data_ptr data, int data_len)
{
	return checksum(data.get(), data_len);
}

bool validate_checksum(const uint8_t* data, int data_len)
{
	assert(data_len >= 0);
	uint8_t cs = 0;
	const uint8_t* p = data;
	for (int i = 0; i < data_len; ++i)
	{
		cs += *p++;
	}
	return (cs == 0);
}

bool validate_checksum(data_ptr data, int data_len)
{
	return validate_checksum(data.get(), data_len);
}

int cal_frame_len(int data_len)
{
	assert(data_len >= 0);
	return sizeof(ni_frame_header) + data_len + sizeof(frame_tailer);
}

frame_ptr new_frame(int frame_len)
{
	assert(frame_len >= 0);
	frame_ptr frame(new uint8_t[frame_len]);
	return frame;
}

int make_frame(uint8_t* buf, int buf_len, uint8_t tfi, const uint8_t* data, int data_len)
{
	assert(data_len >= 0);
	int frame_len = cal_frame_len(data_len);
	if (buf == NULL || buf_len == 0)
	{
		return frame_len;
	}
	if (buf_len < frame_len)
	{
		return -1;
	}
	uint8_t* f = buf;
	ni_frame_header* h = (ni_frame_header*)f;
	uint8_t* d = f + sizeof(ni_frame_header);
	frame_tailer* t = (frame_tailer*)(d + data_len);
	h->preamble = 0x00;
	h->start_code[0] = 0x00;
	h->start_code[1] = 0xFF;
	h->len = data_len + 1;
	h->lcs = checksum(&h->len, 1);
    h->tfi = tfi;
	uint8_t dcs = tfi;
	const uint8_t* dd = data;
	for (int i = 0; i < data_len; ++i)
	{
		*d++ = *dd;
		dcs += *dd++;
	}
	dcs = (~dcs) + 1;
	t->dcs = dcs;
	t->postamble = 0x00;
	return frame_len;
}

int make_frame(frame_ptr & frame, uint8_t tfi, const uint8_t* data, int data_len)
{
	assert(data_len >= 0);
	int frame_len = cal_frame_len(data_len);
	frame = new_frame(frame_len);
	return make_frame(frame.get(), frame_len, tfi, data, data_len);
}

int make_frame(frame_ptr & frame, uint8_t tfi, data_ptr data, int data_len)
{
	assert(data_len >= 0);
	int frame_len = cal_frame_len(data_len);
	frame = new_frame(frame_len);
	return make_frame(frame.get(), frame_len, tfi, data.get(), data_len);
}

}
