#include "pn532_spi.h"
#include <cassert>

namespace pn532
{

int cal_spi_frame_len(int data_len)
{
	return cal_frame_len(data_len) + 1;
}

int make_spi_frame(uint8_t* buf, int buf_len, uint8_t spi_tfi, uint8_t tfi, const uint8_t* data, int data_len)
{
	int frame_len = cal_spi_frame_len(data_len);
	if (buf == NULL || buf_len == 0)
	{
		return frame_len;
	}
	if (buf_len < frame_len)
	{
		return -1;
	}
	buf[0] = spi_tfi;
	int ret = make_frame(buf+1, buf_len-1, tfi, data, data_len);
	if (ret < 0)
	{
		return ret;
	}
	return frame_len;
}

int make_spi_frame(frame_ptr& frame, uint8_t spi_tfi, uint8_t tfi, const uint8_t* data, int data_len)
{
	assert(data_len >= 0);
	int frame_len = cal_spi_frame_len(data_len);
	frame = new_frame(frame_len);
	return make_spi_frame(frame.get(), frame_len, spi_tfi, tfi, data, data_len);
}

int make_spi_frame(frame_ptr& frame, uint8_t spi_tfi, uint8_t tfi, data_ptr data, int data_len)
{
	assert(data_len >= 0);
	int frame_len = cal_spi_frame_len(data_len);
	frame = new_frame(frame_len);
	return make_spi_frame(frame.get(), frame_len, spi_tfi, tfi, data.get(), data_len);
}

}
