#pragma once

#include "pn532.h"

namespace pn532
{

const uint8_t SPI_DW = 0x01;
const uint8_t SPI_DR = 0x03;
const uint8_t SPI_SR = 0x02;

int cal_spi_frame_len(int data_len);
int make_spi_frame(uint8_t* buf, int buflen, uint8_t spi_tfi, uint8_t tfi, const uint8_t* data, int data_len);
int make_spi_frame(frame_ptr& frame, uint8_t spi_tfi, uint8_t tfi, const uint8_t* data, int data_len);
int make_spi_frame(frame_ptr& frame, uint8_t spi_tfi, uint8_t tfi, data_ptr data, int data_len);

}
