CFLAGS=-g -std=c++11
LDFLAGS=

all: get_uid_spi get_uid_i2c

get_uid_spi: get_uid_spi.o pn532_spi.o pn532.o
	g++ -o get_uid_spi get_uid_spi.o pn532_spi.o pn532.o ${LDFLAGS}

get_uid_i2c: get_uid_i2c.o pn532.o
	g++ -o get_uid_i2c get_uid_i2c.o pn532.o ${LDFLAGS}

pn532.o: pn532.cpp
	g++ -c -o pn532.o pn532.cpp ${CFLAGS}

pn532_spi.o: pn532_spi.cpp
	g++ -c -o pn532_spi.o pn532_spi.cpp ${CFLAGS}

get_uid_spi.o: get_uid_spi.cpp
	g++ -c -o get_uid_spi.o get_uid_spi.cpp ${CFLAGS}

get_uid_i2c.o: get_uid_i2c.cpp
	g++ -c -o get_uid_i2c.o get_uid_i2c.cpp ${CFLAGS}

clean:
	rm *.o get_uid_spi get_uid_i2c
