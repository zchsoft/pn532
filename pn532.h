#pragma once

#include <memory>

namespace pn532
{

const uint8_t TFI_H2P = 0xD4;
const uint8_t TFI_P2H = 0xD5;

#pragma pack(1)
struct ni_frame_header
{
	uint8_t preamble;
	uint8_t start_code[2];
	uint8_t len;
	uint8_t lcs;
	uint8_t tfi;
};

struct frame_tailer
{
	uint8_t dcs;
	uint8_t postamble;
};
#pragma pack()

const uint8_t ACK_FRAME[6] = {0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00};
const uint8_t NACK_FRAME[6] = {0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00};
const uint8_t ERROR_FRAME[8] = {0x00, 0x00, 0xFF, 0x01, 0xFF, 0x7F, 0x81, 0x00};

typedef std::shared_ptr<uint8_t[]> byte_ptr;
typedef byte_ptr data_ptr;
typedef byte_ptr frame_ptr;

uint8_t checksum(const uint8_t * data, int data_len);
uint8_t checksum(data_ptr data, int data_len);
bool validate_checksum(const uint8_t * data, int data_len);
bool validate_checksum(data_ptr data, int data_len);
int cal_frame_len(int data_len);
frame_ptr new_frame(int frame_len);
int make_frame(uint8_t* buf, int buf_len, uint8_t tfi, const uint8_t* data, int data_len);
int make_frame(frame_ptr& frame, uint8_t tfi, const uint8_t* data, int data_len);
int make_frame(frame_ptr& frame, uint8_t tfi, data_ptr data, int data_len);

}

