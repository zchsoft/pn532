#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <string.h>
#include "pn532.h"

using namespace pn532;

#define IOCTL(fd, cmd, arg) \
	do\
	{\
		if (ioctl(fd, cmd, arg) == -1)\
		{\
			perror(#cmd);\
			close(fd);\
			return -1;\
		}\
	}\
	while(0)

int init_i2c(const char * dev)
{
	static const ulong addr = 0x24;

	int fd = open(dev, O_RDWR);
	if (fd == -1)
	{
		perror("open device:");
		return -1;
	}
	
	IOCTL(fd, I2C_SLAVE, addr);

	return fd;
}

int send_i2c(int fd, const uint8_t* sendbuf, int sendlen)
{
	usleep(5000);
	return write(fd, sendbuf, sendlen);
}

int recv_i2c(int fd, uint8_t* recvbuf, int recvlen)
{
	usleep(5000);
	return read(fd, recvbuf, recvlen);
}

void print_hex(const uint8_t * buf, int len)
{
	const uint8_t * p = buf;
	for (int i = 0; i < len; ++i)
	{
		printf("%02X ", *p++);
		if ((i+1) % 16 == 0)
		{
			printf("\n");
		}
	}
	if (len % 16 != 0)
	{
		printf("\n");
	}
}

int wait_for_ready(int fd)
{
	uint8_t buf[1];

	while (1)
	{
		usleep(10000);
		if (recv_i2c(fd, buf, 1) != 1)
		{
			printf("recv status error!\n");
			return -1;
		}
		if (buf[0] == 0x01)
			return 0;
	}
	return -1;
}

#define BUF_SIZE 264

int do_cmd(int fd, const uint8_t* cmd, int cmdlen, const char* cmdname, int answerlen)
{
	if (cmdname != NULL)
	{
		printf("do command: %s\n", cmdname);
	}

	if (answerlen < 0)
	{
		answerlen = BUF_SIZE;
	}

	uint8_t buf[BUF_SIZE+1];
	frame_ptr cmd_frame;
	int cmd_frame_len = make_frame(cmd_frame, TFI_H2P, cmd, cmdlen);
	print_hex(cmd_frame.get(), cmd_frame_len);
	if (send_i2c(fd, cmd_frame.get(), cmd_frame_len) != cmd_frame_len)
	{
		perror("write cmd");
		return -1;
	}
	if (wait_for_ready(fd) != 0)
	{
		return -1;
	}
	printf("ready for ack!\n");
	if (recv_i2c(fd, buf, 7) < 0)
	{
		perror("read ack error");
		return -1;
	}
	printf("read ack: ");
	print_hex(buf+1, 6);
	if (wait_for_ready(fd) != 0)
	{
		return -1;
	}
	printf("ready for answer!\n");
	int alen = 0;
	if ((alen = recv_i2c(fd, buf, answerlen+1)) < 0)
	{
		perror("read answer error");
		return -1;
	}
	printf("read answer: ");
	print_hex(buf+1, alen-1);

	return 0;
}

int main(int argc, char *argv[])
{
	int fd = init_i2c("/dev/i2c-1");
	if (fd < 0)
	{
		fprintf(stderr, "init device error!\n");
		return -1;
	}
	
#if 0
	uint8_t gfv_data[] = {0x02};
	if (do_cmd(fd, gfv_data, sizeof(gfv_data), "get fireware version", 13) < 0)
	{
		return -1;
	}
#endif

#if 1
	uint8_t set_normal_mode_data[] = {0x14, 0x01, 0x00, 0x00};
	if (do_cmd(fd, set_normal_mode_data, sizeof(set_normal_mode_data), "set normal mode", 9) < 0)
	{
		return -1;
	}
#endif

#if 1
	uint8_t autopoll_data[] = {0x60, 0xff, 0x02, 0x00, 0x01, 0x02, 0x03, 0x04, 0x10, 0x11, 0x12, 0x20, 0x23};
	if (do_cmd(fd, autopoll_data, sizeof(autopoll_data), "auto poll", 32) < 0)
	{
		return -1;
	}
#endif

#if 0
	uint8_t lpt_data[] = {0x4A, 0x01, 0x00};
	if (do_cmd(fd, lpt_data, sizeof(lpt_data), "list passive target", 32) < 0)
	{
		return -1;
	}
#endif

	close(fd);
	return 0;
}
